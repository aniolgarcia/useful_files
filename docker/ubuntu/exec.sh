#!/bin/bash

docker build . -t ubuntu --build-arg user=$USER
docker run --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -c $HOME:$HOME --name ubuntu ubuntu
