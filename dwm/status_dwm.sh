#!/bin/sh

mybatery ()
{
	echo "`acpi | grep [0-9]+% -E -o`"
}

mydate()
{
	echo `date | grep -E [0-9]{2}:.. -o`
}


mybatterytlp()
{
	echo `tlp stat | grep -m 1 -o -E "[0-9]+\.[0-9]\s\[\%\]"`
}


while xsetroot -name "⌚`mydate`│⏚`mybatery`"
do
	sleep 60
done
