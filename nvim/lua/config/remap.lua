vim.g.mapleader = " "
vim.keymap.set("n", "<leader>fv", vim.cmd.Ex)

-- mou selecció amunt i avall
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

-- posa la línia de sota a continuació conservant la posició del cursor
vim.keymap.set("n", "J", "mzJ`z")

-- centra cursor després de C-d i C-u
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")

-- envia selecció al void register i enganxa
vim.keymap.set("x", "<leader>p", [["_dP]])

-- yank al portaretalls de sistema
vim.keymap.set({"n", "v"}, "<leader>y", [["+y]])
vim.keymap.set("n", "<leader>Y", [["+Y]])



