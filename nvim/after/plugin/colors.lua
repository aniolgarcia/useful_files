function colorConfig(color)
	color = color or "zenbones"
	vim.opt.termguicolors = true
	vim.opt.background = "light"
	vim.cmd.colorscheme(color)
end

function LineNumberColors()
--    vim.api.nvim_set_hl(0, 'LineNrAbove', { fg='blue', bold=true })
--    vim.api.nvim_set_hl(0, 'LineNr', { fg='black', bold=true })
--    vim.api.nvim_set_hl(0, 'LineNrBelow', { fg='magenta', bold=true })
--    vim.api.nvim_set_hl(0, 'LineNr', { bg='#D6CDC9'})
--    vim.cmd('hi! LineNr guibg=grey ctermbg=none')
end

colorConfig()
LineNumberColors()
